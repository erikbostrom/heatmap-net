import numpy as np
import cv2
import keras
from matplotlib import pyplot as plt
import time

class batch_generator(keras.utils.Sequence):
    def __init__(self, img_paths, bbox_arrays, batch_size=32, target_size=(480,480), scaling_factor=16, shuffle=False):
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.img_paths = img_paths
        self.target_size = target_size
        self.scaling_factor = scaling_factor
        self.target_size_scaled = (int(target_size[0]/self.scaling_factor),int(target_size[1]/self.scaling_factor))
        self.bbox_arrays = bbox_arrays
        self._gen_indices()
        self.N = len(img_paths)
        if self.N != len(bbox_arrays):
            ValError("For each img_path there must be one bbox_arrays element!")
        
    def __len__(self):
        return int(np.floor(len(self.img_paths)/self.batch_size))

    def __getitem__(self, batch_index):
        batch_indices = self.indices[batch_index*self.batch_size:(batch_index+1)*self.batch_size]
        img_path_batch = [self.img_paths[k] for k in batch_indices]
        bboxes_batch = [self.bbox_arrays[k] for k in batch_indices]        
        self.x, self.y = self._gen_data(img_path_batch, bboxes_batch)
        return self.x, self.y

    def _gen_indices(self):
        self.indices = np.arange(len(self.img_paths))
        if self.shuffle == True:
            np.random.shuffle(self.indices)

    def _gen_data(self, img_path_batch, bboxes_batch):
        x = np.empty((self.batch_size, *self.target_size, 3), dtype=np.uint8)
        y = np.empty((self.batch_size, *self.target_size_scaled, 2), dtype=np.uint8)
        for i in range(len(img_path_batch)):
            x[i,], img_shape = self._read_image(img_path_batch[i])
            y[i,] = self._gen_heatmap(img_shape, bboxes_batch[i])
        return x, y

    def _read_image(self, img_path):
        img = cv2.imread(img_path)
        return cv2.resize(img, self.target_size, interpolation = cv2.INTER_AREA), img.shape

    def _gen_heatmap(self, img_shape, bbox_array):   
        Nh = img_shape[0]
        Nw = img_shape[1]
        Nc = 1 # so far only one category
        heatmap = np.zeros([Nh,Nw,Nc])
        heatmap_scaled = np.zeros([self.target_size_scaled[0],self.target_size_scaled[1],Nc+1])
        for bbox in bbox_array:
            for i in range(0, Nw):
                for j in range(0, Nh):
                    if i >= int(bbox[0]) and i <= int(bbox[0] + bbox[2]) and j >= int(bbox[1]) and j <= int(bbox[1] + bbox[3]):
                        heatmap[j,i,0] = 1
        heatmap_scaled[:,:,1] = cv2.resize(heatmap[:,:,0], self.target_size_scaled, interpolation=cv2.INTER_NEAREST)
        heatmap_scaled[:,:,0] = 1 - heatmap_scaled[:,:,1]
        return heatmap_scaled

    def visualize(self, indx=0):
        x_img = cv2.cvtColor(self.x[indx,], cv2.COLOR_BGR2RGB)
        plt.subplot(121)
        plt.imshow(x_img)
        plt.subplot(122)
        plt.imshow(self.y[indx,:,:,1])
        plt.show()

    def visualize_all(self):
        for i in range(self.batch_size):
            x_img = cv2.cvtColor(self.x[i,], cv2.COLOR_BGR2RGB)
            plt.subplot(121)
            plt.imshow(x_img)
            plt.subplot(122)
            plt.imshow(self.y[i,:,:,1])
            plt.draw()
            plt.pause(1e-17)
            time.sleep(2)
        plt.show()
