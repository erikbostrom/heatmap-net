import numpy as np
from matplotlib import pyplot as plt
import matplotlib.patches as patches
from pycocotools.coco import COCO
from PIL import Image
import skimage.io as io
import cv2

class coco_data():
    def __init__(self, cocoapi_path, dataset='train2014', cat_nms=['person'], iscrowd=None):
        self.coco=COCO('{}/annotations/instances_{}.json'.format(cocoapi_path, dataset))
        self.img_folder='{}/images/{}'.format(cocoapi_path, dataset)
        self.dataset = dataset
        self.cat_nms = cat_nms
        self.cat_ids = self.coco.getCatIds(catNms=self.cat_nms)
        self.img_ids = self.coco.getImgIds(catIds=self.cat_ids)
        self.coco_imgs = self.coco.loadImgs(ids=self.img_ids)
        self.iscrowd = iscrowd
        self.ann_ids = self.coco.getAnnIds(imgIds=self.img_ids, catIds=self.cat_ids, iscrowd=self.iscrowd)
        self.coco_anns = self.coco.loadAnns(self.ann_ids)
        self._get_img_paths()
        self._get_bbox_arrays()
        
    def __str__(self):
        return "COCO data. Dataset: {},  Loaded categories: {}.".format(self.dataset, self.cat_nms)
    
    def __getitem__(self,indx):
        return self.img_paths[indx], self.bbox_arrays[indx]

    def __len__(self):
        return len(self.img_paths)

    def _get_img_paths(self):
        self.img_paths = []
        for img in self.coco_imgs:
            self.img_paths.append(self.img_folder+"/"+img['file_name'])
        return self.img_paths
    
    def _get_bbox_arrays(self):
        self.bbox_arrays = []
        for img in self.coco_imgs:
            ann_ids = self.coco.getAnnIds(img['id'], catIds=self.cat_ids, iscrowd=self.iscrowd)
            anns = self.coco.loadAnns(ann_ids)
            bbox_array = []
            for ann in anns:
                bbox_array.append(ann['bbox'])
            self.bbox_arrays.append(bbox_array)
        return self.bbox_arrays

    def get_ann_ids(self):
        return self.ann_ids

    def get_img_infos(self):
        return self.coco_imgs

    def get_img_anns(self):
        return self.coco_anns
    
class coco_img():
    def __init__(self, img_info, img_annotations, cocoapi_img_folder, scaling=16):
        self.img_info = img_info
        self.annotations = img_annotations
        self.img_path = '{}/{}'.format(cocoapi_img_folder, self.img_info['file_name'])
        self.img = read_image(self.img_path)
        self.scaling
        self.bbox_array = []
        self.rects = []
        for ann in self.annotations:
            bbox = ann['bbox']
            self.bbox_array.append(bbox)
            self.rects.append(patches.Rectangle((bbox[0],bbox[1]),bbox[2],bbox[3],
                                                linewidth=1,edgecolor='r',facecolor='none'))
                    
        self._load_cat_maps()
        
    def __str__(self):
        return self.img_info
            
    def get_data(self):
        return self.img

    def visualize(self):
        plt.axis('off')
        plt.imshow(self.img)        
        for rect in self.rects:
            plt.gca().add_patch(rect)
        plt.title('input\n({})\nsize={}'. format(self.img_info['file_name'], self.img.shape))
        plt.show()
