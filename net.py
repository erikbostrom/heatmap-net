import numpy as np
import keras
from keras.models import Model
from keras.layers import Conv2D, Input
from keras.applications import VGG16
from keras.optimizers import SGD
from matplotlib import pyplot as plt
import cv2

class cnn_base():
    def __init__(self, weights='imagenet', input_shape=(224, 224, 3)):
        self.weights = weights
        self.input_shape = input_shape
        self.model = self._vgg16_model()

    def _vgg16_model(self, outp_layer='block5_conv1'):
        vgg_model = VGG16(weights=self.weights, include_top=False, input_shape=self.input_shape)
        for layer in vgg_model.layers: layer.trainable = False
        return Model(inputs=vgg_model.input, outputs=vgg_model.get_layer(outp_layer).output)

    def get_model(self):
        return self.model

    def summary(self):
        self.model.summary()
        
    def predict(self,img_data):
        return self.model.predict(np.expand_dims(img_data, axis=0))
        
    def load_weights(self, filepath, by_name=True):
        self.model.load_weights(filepath, by_name)
        print("weights were loaded from file: %s" % (filepath))

class rpn():
    def __init__(self, n_categories=1, input_shape=(None,None,3)):
        self.n_categories = n_categories
        self.input_shape = input_shape
        cnn = cnn_base(input_shape=self.input_shape)
        self.cnn_model = cnn.get_model()
        self.cnn_model.name = "cnn_model"
        self.rpn_model = Model(inputs=self.cnn_model.input, outputs=self._rpn_net())
        self.rpn_model.name = "rpn_model"

    def summary(self):
        self.rpn_model.summary()
        
    def _rpn_net(self):
        x = self.cnn_model(self.cnn_model.input)
        x = Conv2D(512, (3, 3), activation='relu', padding='same', name='rpn_conv1')(x)
        x = Conv2D(512, (1, 1), activation='relu', padding='same', name='rpn_conv2')(x)
        x = Conv2D(self.n_categories+1, (1, 1), activation='softmax', padding='same', name='scores')(x)
        return x

    def train(self, train_generator, validation_generator, loss='categorical_crossentropy', optimizer='sgd', metrics=['acc']):
        self.rpn_model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
        self.rpn_model.fit_generator(generator=train_generator, validation_data=validation_generator, verbose=1)

    def predict(self, img_data):
        return self.rpn_model.predict(np.expand_dims(img_data, axis=0))

    def save_weights(self, filepath):
        self.rpn_model.save_weights(filepath)
        print("weights were saved to file: %s" % (filepath))

    def load_weights(self, filepath, by_name=True):
        self.rpn_model.load_weights(filepath, by_name)
        print("weights were loaded from file: %s" % (filepath))
        
