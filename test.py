import numpy as np
from data import *
from batch_generator import *
from net import *
import utils

CATEGORIES   = ['person']
WEIGHTS_FILE = 'rpn_weights.h5'
INPUT_SHAPE  = (256,256,3)
COCO_PATH    = '/home/erik/Data/cocoapi'
DATA_SET     = 'train2014'


# Create the RPN model
rpn_model = rpn( n_categories=len(CATEGORIES),
                 input_shape=INPUT_SHAPE)

# load weights of trained model
rpn_model.load_weights(WEIGHTS_FILE)

# diplay model info
rpn_model.summary()

# load data to predict
test_data = coco_data( cocoapi_path=COCO_PATH,
                       dataset=DATA_SET,
                       cat_nms=CATEGORIES,
                       iscrowd=False)

img_paths_test, img_bboxes_test = test_data[0:10]

# predict
img_data = utils.read_img(img_paths_test[2],INPUT_SHAPE)

print(rpn_model.predict(img_data))

#plt.imshow(img_data)
#plt.show()
