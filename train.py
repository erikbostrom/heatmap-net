import numpy as np
from data import *
from batch_generator import *
from net import *

# Configuration
BATCH_SIZE   = 32
EPOCHS       = 1
DATASET_SIZE = 0.25
COCO_PATH    = '/home/erik/Data/cocoapi'
DATA_SET     = 'train2014'
CATEGORIES   = ['person']
OPTIMIZER    = 'sgd'
LOSS         = 'categorical_crossentropy'
INPUT_SHAPE  = (256,256,3)
SCALING      = 16
WEIGTHS_FILE = 'rpn_weights.h5'
IS_CROWD     = False


# Load coco data
train_data = coco_data( cocoapi_path=COCO_PATH,
                        dataset=DATA_SET,
                        cat_nms=CATEGORIES,
                        iscrowd=IS_CROWD)

# Get paths to imgages and corresponding bounding boxes from the coco data
n_images = int(len(train_data)*DATASET_SIZE)
n_train = int(n_images*0.7)
img_paths_train, img_bboxes_train = train_data[0:n_train]
img_paths_valid, img_bboxes_valid = train_data[n_train:n_images]

# Create a train generator object
train_generator = batch_generator( img_paths_train,
                                   img_bboxes_train,
                                   batch_size=BATCH_SIZE,
                                   target_size=(INPUT_SHAPE[0],INPUT_SHAPE[1]),
                                   scaling_factor=SCALING)

# Create a validation genarator object
validation_generator = batch_generator( img_paths_valid,
                                        img_bboxes_valid,
                                        batch_size=BATCH_SIZE,
                                        target_size=(INPUT_SHAPE[0],INPUT_SHAPE[1]),
                                        scaling_factor=SCALING)

# Create the RPN model
rpn_model = rpn( n_categories=len(CATEGORIES),
                 input_shape=INPUT_SHAPE)

# Train the RPN
rpn_model.train(train_generator=train_generator,
                validation_generator=validation_generator,
                optimizer=OPTIMIZER,
                loss=LOSS)

# Save trained weights for the RPN
rpn_model.save_weights(WEIGTHS_PATH)
