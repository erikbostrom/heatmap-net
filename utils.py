import cv2

def read_img(img_path, shape):
    img = cv2.resize(cv2.imread(img_path), (shape[0],shape[1]), interpolation = cv2.INTER_AREA)
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
